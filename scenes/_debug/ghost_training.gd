extends Node2D
@onready var ground = $Ground
@onready var player = $Player

func _ready():
	ground.can_interact = true

func _on_ground_action_performed(action: String):
	if action == "harvest":
		player.carring_pumpking()
