extends Node2D
@onready var introduction = $Introduction
@onready var defeat = $Defeat
@onready var victory = $Victory
@onready var cycleTimer = $World/Cycle
@onready var player = $World/Player
@onready var ghostTimer = $World/GhostTimer
@onready var hud = $HUD
@onready var hudCycleCounter = $HUD/CycleLabel
@onready var choicePanel = $ChoicePanel
@onready var world = $World
@onready var music_day = $MusicDay
@onready var music_night = $MusicNight
@onready var music_victory = $MusicVictory

const goal = 100
const limit_cycle: int = 60
const cycles_by_day:int = 6
const cycles_by_daytime = 2

@export var debug_enabled = false
var score

func _ready():
	hud.set_cycles_by_day(cycles_by_day)

func new_game():
	world.reset()
	introduction.hide()
	cycleTimer.start()
	ghostTimer.start()
	score = 0
	hud.reset(goal)
	if debug_enabled:
		hudCycleCounter.show()
	hud.set_days_left(int(limit_cycle/cycles_by_day))
	player.reset()
	music_victory.stop()
	music_day.volume_db = -15
	music_day.play()

func _on_introduction_new_game_requested():
	new_game()

func game_won():
	game_finished()
	victory.show()
	var tween = get_tree().create_tween()
	tween.tween_property(music_day, "volume_db", -80, 1)
	tween.tween_property(music_night, "volume_db", -80, 2)
	music_day.stop()
	music_night.stop()
	music_victory.play()

func game_lost():
	game_finished()
	defeat.show()
	var tween = get_tree().create_tween()
	tween.tween_property(music_day, "volume_db", -80, 1)
	tween.tween_property(music_night, "volume_db", -80, 2)
	music_day.stop()
	music_night.stop()

func game_finished():
	cycleTimer.stop()
	ghostTimer.stop()
	hud.hide()

func _on_victory_introduction_requested():
	victory.hide()
	introduction.show()

func _on_defeat_introduction_requested():
	defeat.hide()
	introduction.show()

func _on_world_cycle_incremented(nbCycles):
	hud.update_cycle(nbCycles)
	if nbCycles == limit_cycle:
		game_lost()
	if nbCycles % cycles_by_day == cycles_by_daytime:
		world.change_to_night()
		var tween = get_tree().create_tween()
		tween.tween_property(music_day, "volume_db", -80, 1)
		tween.tween_property(music_night, "volume_db", -15, 2)
		music_night.play()
	if nbCycles % cycles_by_day == 0:
		hud.change_day()
		world.change_to_day()
		choicePanel.reset(nbCycles / cycles_by_day)
		choicePanel.show()
		cycleTimer.stop()
		var tween = get_tree().create_tween()
		tween.tween_property(music_night, "volume_db", -80, 1)
		tween.tween_property(music_day, "volume_db", -15, 2)
		music_day.play()

func _on_world_score_incremented():
	score += 1
	hud.update_score(score, goal)
	if score == goal:
		game_won()

func _on_choice_panel_choice_confirmed(boost_type):
	choicePanel.hide()
	cycleTimer.start()
	world.apply_boost(boost_type)
	hud.enable_or_increase(boost_type)
