extends Path2D
@onready var day = $PathFollow2D/Date/Day
@onready var animation_player = $AnimationPlayer

func set_date(date_number):
	day.text = str(date_number)

func fall():
	animation_player.play("fall")

func _on_animation_player_animation_finished(_anim_name):
	queue_free()
