extends CanvasLayer
signal introduction_requested
@onready var crowd = $Crowd

func _on_button_pressed():
	introduction_requested.emit()

func _on_visibility_changed():
	if visible == true:
		crowd.play()
