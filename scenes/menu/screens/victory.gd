extends CanvasLayer
signal introduction_requested
@onready var crowd = $Crowd

func _on_back_to_menu_pressed():
	introduction_requested.emit()

func _on_visibility_changed():
	if visible == true:
		crowd.play()
