extends CanvasLayer

signal new_game_requested

func _on_play_pressed():
	new_game_requested.emit()
