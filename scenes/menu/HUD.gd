extends CanvasLayer
@onready var cycle_label = $CycleLabel
@onready var pumpkin_score = $PumpkinScore
@onready var cart_empty = $"Cart-empty"
@onready var cart_33 = $"Cart-33"
@onready var cart_66 = $"Cart-66"
@onready var cart_full = $"Cart-full"
@onready var calendar = $Calendar
@onready var attack = $Attack
@onready var fertilizer = $Fertilizer
@onready var speed = $Speed
@onready var golden_sickle = $"Golden-sickle"
@onready var pickaxe = $Pickaxe
@onready var watercan = $Watercan

const boosts_default = {
	"attack": 0,
	"fertilizer": 0,
	"speed": 0,
	"golden_sickle": 0,
	"pickaxe": 0,
	"watering_can": 0
}
var cycles_by_day
var boosts

func update_cycle(nbCycles):
	cycle_label.text = str(nbCycles)+" cycles"

	# Compute day progression for the clock
	var cycle_for_the_current_day = nbCycles % cycles_by_day
	var percent = (cycle_for_the_current_day * 100) / cycles_by_day
	calendar.set_time_progress(percent)

func reset(goal):
	update_score(0, goal)
	update_cycle(0)
	boosts = boosts_default.duplicate()
	attack.hide()
	speed.hide()
	fertilizer.hide()
	golden_sickle.hide()
	pickaxe.hide()
	watercan.hide()
	show()

func update_score(score, goal):
	pumpkin_score.text = str(score)+"/"+str(goal)
	var progress = (score * 100) / goal

	hide_cart()
	if progress <= 33:
		cart_33.show()
	elif progress <= 66:
		cart_66.show()
	elif progress <= 100:
		cart_full.show()

func hide_cart():
	cart_empty.hide()
	cart_33.hide()
	cart_66.hide()
	cart_full.hide()

func change_day():
	calendar.new_day()

func set_days_left(day_number: int):
	calendar.set_day(day_number)

func set_cycles_by_day(init_cycles_by_day):
	cycles_by_day = init_cycles_by_day

func enable_or_increase(boost_type):
	boosts[boost_type] += 1
	match(boost_type):
		"attack":
			attack.show()
			attack.get_node("Level").text = "+" + str(boosts[boost_type])
		"speed":
			speed.show()
			speed.get_node("Level").text = "+" + str(boosts[boost_type])
		"fertilizer":
			fertilizer.show()
			fertilizer.get_node("Level").text = "+" + str(boosts[boost_type])
		"golden_sickle":
			golden_sickle.show()
		"pickaxe":
			pickaxe.show()
		"watering_can":
			watercan.show()
