extends CanvasLayer
signal choice_confirmed

@onready var icon_choice_left = $IconChoiceLeft
@onready var icon_choice_right = $IconChoiceRight
@onready var label_choice_left = $LabelChoiceLeft
@onready var label_choice_right = $LabelChoiceRight
@onready var timer = $Timer

var boosts = {
	"attack": {
		"sprite": preload("res://assets/sprites/boost/attack.png"),
		"label": "Attack +",
		"scale": 1,
		"unique": false
	},
	"fertilizer": {
		"sprite": preload("res://assets/sprites/boost/fertilizer.png"),
		"label": "Plants grow\nfaster",
		"scale": 1,
		"unique": false
	},
	"speed": {
		"sprite": preload("res://assets/sprites/boost/speed.png"),
		"label": "Move Speed +",
		"scale": 1,
		"unique": false
	},
	"golden_sickle": {
		"sprite": preload("res://assets/sprites/tools/golden_sickle.png"),
		"label": "Magic\nHarvest",
		"scale": 2,
		"unique": true
	},
	"watering_can": {
		"sprite": preload("res://assets/sprites/tools/watercan.png"),
		"label": "Watered Plants\nGrow Faster",
		"scale": 2,
		"unique": true
	},
	"pickaxe": {
		"sprite": preload("res://assets/sprites/tools/pickaxe.png"),
		"label": "Break Rocks\nFaster",
		"scale": 2,
		"unique": true
	}
}

var left_boost
var right_boost

func reset(dayNumber):
	left_boost = null
	right_boost = null
	
	var boosts_names = boosts.keys()
	boosts_names.sort()

	var object_id = null
	if dayNumber < 3:
		object_id = boosts_names.bsearch("golden_sickle")
		boosts_names.pop_at(object_id)
		object_id = boosts_names.bsearch("watering_can")
		boosts_names.pop_at(object_id)
		object_id = boosts_names.bsearch("pickaxe")
		boosts_names.pop_at(object_id)

	left_boost = boosts_names.pick_random()
	var left_boost_id = boosts_names.bsearch(left_boost)
	boosts_names.pop_at(left_boost_id)
	
	right_boost = boosts_names.pick_random()
	
	icon_choice_left.texture = boosts[left_boost]["sprite"]
	icon_choice_right.texture = boosts[right_boost]["sprite"]
	
	resize_texture(icon_choice_left, boosts[left_boost]["scale"])
	resize_texture(icon_choice_right, boosts[right_boost]["scale"])
	
	label_choice_left.text = boosts[left_boost]["label"]
	label_choice_right.text = boosts[right_boost]["label"]

func _on_area_choice_left_input_event(_viewport, event, _shape_idx):
	choose(event, true)

func _on_area_choice_right_input_event(_viewport, event, _shape_idx):
	choose(event, false)

func choose(event, is_left):
	if event.is_action_pressed("click") && timer.is_stopped():
		var chosen_boost = left_boost if is_left else right_boost
		choice_confirmed.emit(chosen_boost)
		clear_unique_boost(chosen_boost)

func _on_visibility_changed():
	if visible:
		timer.start()

func resize_texture(sprite, scale):
	sprite.scale = Vector2(scale, scale)

func clear_unique_boost(boost):
	if boosts[boost]["unique"]:
		boosts.erase(boost)
