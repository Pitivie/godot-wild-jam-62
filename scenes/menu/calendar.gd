extends Node2D
@onready var woodenboard = $Woodenboard
@onready var needle = $Needle

var page_scene = preload("res://scenes/menu/date_paper.tscn")
var day
func set_time_progress(percent):
	needle.rotation_degrees = ((((percent + 25) % 100) * 180) /  100) - 90

func set_day(init_day):
	day = init_day
	var page = page_scene.instantiate()
	add_child(page)
	page.set_date(day)

	day -= 1
	if day > 0:
		add_page(day)

func add_page(day_number):
	var page = page_scene.instantiate()
	add_child(page)
	# Set new at the second to last
	move_child(page, 3)
	page.set_date(day_number)

func new_day():
	var page = get_child(get_child_count()-1)
	page.fall()
	day -= 1
	if day > 0:
		add_page(day)
