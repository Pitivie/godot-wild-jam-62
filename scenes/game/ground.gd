extends TileMap
@onready var ground = $"."
@onready var sickle = $Sickle
@onready var watering_can = $WateringCan
@onready var hand = $Hand
@onready var pickaxe = $Pickaxe

signal score_incremented
signal action_performed

const LAYER_DIRT = 0
const LAYER_PLANT = 1
const LAYER_PREVIEW = 2
const SOURCE_DIRT = 0
const SOURCE_PLANT = 1
const GROUND_DIRT = Vector2i(0, 0)
const GROUND_DRY_SOIL = Vector2i(1, 0)
const GROUND_WET_SOIL = Vector2i(1, 1)
const GROUND_ROCK1 = Vector2i(0, 1)
const GROUND_ROCK2 = Vector2i(0, 2)
const GROUND_ROCK3 = Vector2i(0, 3)
const GROUND_ROCK4 = Vector2i(1, 3)
const GROUND_ROCK5 = Vector2i(1, 2)
const ROCKS = [GROUND_ROCK1, GROUND_ROCK2, GROUND_ROCK3, GROUND_ROCK4, GROUND_ROCK5]
const ATLAS_NULL = Vector2i(-1, -1)
const PLANT_SEED = Vector2i(0, 0)
const PLANT_SEEDLING = Vector2i(1, 0)
const PLANT_GROWN = Vector2i(2, 0)

const PUMPKIN_GROWING_SPEED = 10
const FERTILIZER_BOOST = 10
const WATERING_BOOST = 10

var rocks: Dictionary
var has_pickaxe = false
var has_watering_can = false
var can_interact = false
var plants_maturation: Dictionary
var boost_fertilizer_level: int

func _input(event):
	if event.is_action_pressed("click"):
		var mouse_map_position = get_mouse_map_position()
		var dirt_cell = get_dirt_layer_cell(mouse_map_position)
		var plant_cell = get_plant_layer_cell(mouse_map_position)
		if dirt_cell==GROUND_DIRT:
			sow(mouse_map_position)
			return
		if plant_cell==PLANT_GROWN:
			harvest(mouse_map_position)
			return
		if has_watering_can && dirt_cell==GROUND_DRY_SOIL:
			water(mouse_map_position)
			return
		if dirt_cell in ROCKS:
			hit_rock(mouse_map_position)

	if can_do_action():
		var mouse_map_position = get_mouse_map_position()
		var dirt_cell = get_dirt_layer_cell(mouse_map_position)
		var plant_cell = get_plant_layer_cell(mouse_map_position)
		
		#seed preview
		if dirt_cell == GROUND_DIRT:
			ground.set_cell(LAYER_PREVIEW, mouse_map_position, SOURCE_PLANT, PLANT_SEED, 1)
			return 

		#harvest preview
		if plant_cell == PLANT_GROWN:
			sickle.show()
			return 

		#pickaxe or hand preview
		if dirt_cell in ROCKS:
			if has_pickaxe:
				pickaxe.show()
			else:
				hand.show()
			return

		#watering preview
		if has_watering_can && dirt_cell == GROUND_DRY_SOIL:
			watering_can.show()

func get_mouse_map_position():
	return ground.local_to_map(ground.get_local_mouse_position())

func get_dirt_layer_cell(cell):
	return ground.get_cell_atlas_coords(LAYER_DIRT, cell)

func get_plant_layer_cell(cell):
	return ground.get_cell_atlas_coords(LAYER_PLANT, cell)

func sow(cell):
	if !can_do_action():
		return
	ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_DRY_SOIL)
	ground.set_cell(LAYER_PLANT, cell, SOURCE_PLANT, PLANT_SEED)
	plants_maturation[cell] = 0
	action_performed.emit("sow")

func water(cell):
	if !can_do_action():
		return
	ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_WET_SOIL)
	watering_can.run()

func harvest(cell):
	if !can_do_action():
		return
	ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_DIRT)
	ground.set_cell(LAYER_PLANT, cell, SOURCE_PLANT, ATLAS_NULL)
	score_incremented.emit()
	action_performed.emit("harvest")
	sickle.run()

func reset():
	plants_maturation = {}
	boost_fertilizer_level = 0
	ground.clear_layer(LAYER_PLANT)
	var ground_cells = ground.get_used_cells(LAYER_DIRT)
	for ground_cell in ground_cells:
		ground.set_cell(LAYER_DIRT, ground_cell, SOURCE_DIRT, GROUND_DIRT)

	# Init rock area
	rocks = {}
	for rock in 100:
		# Determine random coordonne
		var cell = Vector2i(randi_range(10, 19), randi_range(5, 11))
		# Set life rock on the dictionnary
		rocks[cell] = 15
		# Set rock on the map
		ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_ROCK1)

func grow_plants():
	var plants = ground.get_used_cells(LAYER_PLANT)
	for plant in plants:
		if !plants_maturation.has(plant) || plants_maturation[plant] >= 100:
			continue

		var old_maturation = plants_maturation[plant]
		plants_maturation[plant] += PUMPKIN_GROWING_SPEED + (boost_fertilizer_level * FERTILIZER_BOOST)
		if get_dirt_layer_cell(plant) == GROUND_WET_SOIL:
			plants_maturation[plant] += WATERING_BOOST
		var new_maturation = plants_maturation[plant]

		if old_maturation < 50 && new_maturation >= 50:
			ground.set_cell(LAYER_PLANT, plant, SOURCE_PLANT, PLANT_SEEDLING)

		if old_maturation < 100 && new_maturation >= 100:
			ground.set_cell(LAYER_PLANT, plant, SOURCE_PLANT, PLANT_GROWN)

func can_do_action():
	return can_interact && !watering_can.is_running() && !sickle.is_running()

func dry_tiles_on_the_morning():
	var dirt_cells = ground.get_used_cells_by_id(LAYER_DIRT, SOURCE_DIRT, GROUND_WET_SOIL)
	for dirt_cell in dirt_cells:
		ground.set_cell(LAYER_DIRT, dirt_cell, SOURCE_DIRT, GROUND_DRY_SOIL)

func get_pumpkings() -> Array[Vector2i]:
	return ground.get_used_cells_by_id(1, 1, Vector2(2, 0), 0)

func get_maturation():
	return plants_maturation.values().reduce(func(accum, number): return accum + number, 0)

func get_max_maturation():
	return (get_used_cells_by_id(0, 0, Vector2i(0,0)).size() + get_used_cells_by_id(0, 0, Vector2i(1,0)).size() +get_used_cells_by_id(0, 0, Vector2i(1,1)).size()) * 100

func use_golden_sickle():
	sickle.use_golden_sickle()

func increase_boost_fertilizer_level():
	boost_fertilizer_level += 1

func hit_rock(cell):
	if !can_do_action():
		return

	if has_pickaxe:
		rocks[cell] -= 5
		pickaxe.run()
	else:
		rocks[cell] -= 1
		hand.run()

	if rocks[cell] <= 0:
		ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_DIRT)
	elif rocks[cell] <= 3:
		ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_ROCK5)
	elif rocks[cell] <= 6:
		ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_ROCK4)
	elif rocks[cell] <= 9:
		ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_ROCK3)
	elif rocks[cell] <= 12:
		ground.set_cell(LAYER_DIRT, cell, SOURCE_DIRT, GROUND_ROCK2)
