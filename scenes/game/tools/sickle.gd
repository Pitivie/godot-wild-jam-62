extends Node2D
@onready var animation_player = $AnimationPlayer
@onready var iron = $Marker2D/Iron
@onready var golden = $Marker2D/Golden
@onready var sound_effect = $SoundEffect

func _input(_event):
	position = get_viewport().get_mouse_position()
	position.y += 6
 
func run():
	animation_player.play("harvest")
	sound_effect.play("leque" + str(randi() % 2))

func is_running():
	return animation_player.is_playing()

func use_golden_sickle():
	iron.hide()
	golden.show()

func _on_animation_player_animation_finished(anim_name):
	hide()
