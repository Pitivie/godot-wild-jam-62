extends Node2D
@onready var timer = $Timer
@onready var animation_player = $AnimationPlayer

func _input(_event):
	position = get_viewport().get_mouse_position()

func run():
	timer.start()
	animation_player.play("wet")

func is_running():
	return !timer.is_stopped()
