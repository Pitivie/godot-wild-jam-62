extends Node2D
@onready var animation_player = $AnimationPlayer

func _input(_event):
	position = get_viewport().get_mouse_position()
	position.y -= 8

func run():
	animation_player.play("hit")

func is_running():
	return animation_player.is_playing()
