extends CharacterBody2D
@onready var animation = $Animation
@onready var attack = $Attack
@onready var pumpking = $Pumpking
@onready var point_light_2d = $PointLight2D
@onready var sound_effect = $SoundEffect

const speed = 1500
const damage_point = 1

var screen_size
var enemy
var has_pumpking = false
var has_golden_sickle = false
var boost_speed_level: int
var boost_attack_level: int

func _ready():
	screen_size = get_viewport_rect().size
	play_idle()

func reset():
	boost_speed_level = 0
	boost_attack_level = 0
	position = Vector2(130, 120)

func get_input():
	var input_direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	velocity = input_direction * ( speed + (boost_speed_level * 1000)) 

func _physics_process(delta):
	get_input()
	velocity = velocity * delta
	position = position.clamp(Vector2.ZERO, screen_size)

	if velocity.x > 0:
		animation.flip_h = true
	elif velocity.x < 0:
		animation.flip_h = false

	if velocity != Vector2.ZERO:
		play_run()
	move_and_slide()

func play_idle():
	animation.play("default")
	animation.offset = Vector2.ZERO

func play_run():
	animation.play("run")
	animation.offset.y = -6
	if !sound_effect.is_playing():
		sound_effect.play("walk" + str(randi() % 2))

func play_action():
	animation.play("action")
	animation.offset = Vector2.ZERO

func _on_animation_animation_finished():
	play_idle()

func _on_agressive_body_entered(body):
	if body.is_in_group("ghosts"):
		attack.start()
		enemy = body

func _on_agressive_body_exited(body):
	if body.is_in_group("ghosts"):
		attack.stop()

func _on_attack_timeout():
	enemy.hurt(damage_point + boost_attack_level)

func carring_pumpking():
	has_pumpking = true
	pumpking.show()

func unload_pumpking():
	has_pumpking = false
	pumpking.hide()

func increase_boost_speed_level():
	boost_speed_level += 1

func increase_boost_attack_level():
	boost_attack_level += 1

func light_turn_off():
	point_light_2d.enabled = false

func light_turn_on():
	point_light_2d.enabled = true
