extends Area2D

signal score_incremented

func _on_body_entered(body):
	if body.name == "Player" and body.has_pumpking:
		body.unload_pumpking()
		score_incremented.emit()
