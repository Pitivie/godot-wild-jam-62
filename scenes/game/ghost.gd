extends CharacterBody2D
@onready var death = $Death
@onready var explosion = $Explosion
@onready var animated_sprite_2d = $AnimatedSprite2D
@onready var animation_player = $AnimationPlayer

var target: Vector2
const speed = 600
var life = 5
var ground: TileMap

signal pumpking_eaten

func _physics_process(delta):
	velocity = position.direction_to(target) * speed * delta
	if position.distance_to(target) > 4:
		move_and_slide()
	else:
		if there_is_plant():
			if death.is_stopped():
				pumpking_eaten.emit(target)
				die()
		else:
			pick_target()

func there_is_plant() -> bool:
	var cell = ground.local_to_map(position)
	if ground.get_cell_atlas_coords(1, cell) == Vector2i(-1, -1):
		return false
	else:
		return true

func set_ground(init_ground):
	ground = init_ground

func pick_target():
	var plants = ground.get_pumpkings()
	if plants.size() == 0:
		var random_target = Vector2i.ZERO
		random_target.x = randi_range(0, ProjectSettings.get_setting("display/window/size/viewport_width"))
		random_target.y = randi_range(0, ProjectSettings.get_setting("display/window/size/viewport_height"))
		set_target(random_target)
	else:
		# Get a random target
		var plant = plants.pick_random()
		var plant_target = ground.map_to_local(plant)
		set_target(plant_target)

func set_target(init_target: Vector2):
	target = init_target

func _on_death_timeout():
	queue_free()

func die():
	death.start()
	explosion.emitting = true
	animated_sprite_2d.hide()

func hurt(damage: int):
	animated_sprite_2d.play("hurt")
	animation_player.play("hurt")
	life -= damage
	if life <= 0:
		die()

func _on_animated_sprite_2d_animation_finished():
	if animated_sprite_2d.animation == "hurt":
		animated_sprite_2d.play("move")
