extends Node2D
@onready var ground = $Ground
@onready var sickle = $Ground/Sickle
@onready var hand = $Ground/Hand
@onready var pickaxe = $Ground/Pickaxe
@onready var watering_can = $Ground/WateringCan
@onready var player = $Player
var ghost_scene = preload("res://scenes/game/ghost.tscn")

signal cycle_incremented
signal score_incremented

const max_distance_interaction = 4
var nbCycles
var previous_mouse_map_position = null
var is_daytime

func _input(_event):
	var mouse_map_position = ground.local_to_map(ground.get_local_mouse_position())
	var player_map_position = ground.local_to_map(player.position)
	var distance = vector2i_distance_to(mouse_map_position, player_map_position)
	
	if distance < max_distance_interaction && player.has_pumpking == false:
		ground.can_interact = true
		
		#clean old preview
		if previous_mouse_map_position != null && previous_mouse_map_position != mouse_map_position:
			ground.set_cell(ground.LAYER_PREVIEW, previous_mouse_map_position, ground.SOURCE_PLANT, ground.ATLAS_NULL)
			sickle.hide()
			watering_can.hide()
			hand.hide()
			pickaxe.hide()
		previous_mouse_map_position = mouse_map_position
	else:
		ground.can_interact = false
		ground.clear_layer(ground.LAYER_PREVIEW)
		sickle.hide()
		watering_can.hide()
		hand.hide()
		pickaxe.hide()

func get_dirt_layer_cell(cell):
	return ground.get_cell_atlas_coords(ground.LAYER_DIRT, cell)

func vector2i_distance_to(from, to):
	var dx = to.x - from.x
	var dy = to.y - from.y
	var distance = sqrt(dx * dx + dy * dy)
	return distance

func reset():
	ground.reset()
	nbCycles = 0
	is_daytime = true
	change_to_day()
	var ghosts = get_tree().get_nodes_in_group("ghosts")
	for ghost in ghosts:
		ghost.queue_free()
	give_player_pickaxe()

func _on_cycle_timeout():
	nbCycles = nbCycles+1
	cycle_incremented.emit(nbCycles)
	ground.grow_plants()

func _on_ground_score_incremented():
	if player.has_golden_sickle:
		score_incremented.emit()
	else:
		player.carring_pumpking()

func _on_ground_action_performed(_action: String):
	player.play_action()

func _on_ghost_timer_timeout():
	if is_daytime == true:
		var ghosts = get_tree().get_nodes_in_group("ghosts")
		for ghost in ghosts:
			ghost.die()
		return

	var pumpkins = ground.get_pumpkings()
	if pumpkins.size() == 0:
		return

	var probability_to_spawn = (ground.get_maturation() * 100) / ground.get_max_maturation()
	if randi_range(0, 100) > probability_to_spawn:
		return

	# Instanciate GhostNode and Spawner
	var ghost = ghost_scene.instantiate()
	var ghost_spawn_location = get_node("GhostSpawner/GhostPath/GhostSpawnLocation")

	# Set random position to ghost
	ghost_spawn_location.progress_ratio = randf()
	ghost.position = ghost_spawn_location.position

	# Get a random target
	ghost.set_ground(ground)
	ghost.pick_target()

	# Add node
	add_child(ghost)
	ghost.pumpking_eaten.connect(_on_ghost_pumpking_eaten)

func _on_ghost_pumpking_eaten(target: Vector2):
	var cell = ground.local_to_map(target)
	ground.set_cell(ground.LAYER_PLANT, cell)
	ground.set_cell(ground.LAYER_DIRT, cell, ground.SOURCE_DIRT, ground.GROUND_DIRT)

func change_to_day():
	is_daytime = true
	var tween = get_tree().create_tween()
	tween.tween_property(self, "modulate", Color(1, 1, 1, 1), 1)
	ground.dry_tiles_on_the_morning()
	player.light_turn_off()

func change_to_night():
	is_daytime = false
	var tween = get_tree().create_tween()
	tween.tween_property(self, "modulate", Color(0.5, 0.5, 0.5, 1.0), 1)
	player.light_turn_on()

func _on_cart_score_incremented():
	score_incremented.emit()

func give_player_golden_sickle():
	player.has_golden_sickle = true
	ground.use_golden_sickle()

func give_player_pickaxe():
	ground.has_pickaxe = true

func apply_boost(boost_type):
	match(boost_type):
		"attack":
			player.increase_boost_attack_level()
		"speed":
			player.increase_boost_speed_level()
		"fertilizer":
			ground.increase_boost_fertilizer_level()
		"golden_sickle":
			give_player_golden_sickle()
		"watering_can":
			ground.has_watering_can = true
		"pickaxe":
			give_player_pickaxe()
